package declean.spark.service.handle;

import static spark.Spark.port;

import declean.spark.model.Person;
import declean.spark.service.DefaultRestApiService;
import declean.spark.service.RestApiService;
import spark.Service;

public class ServiceAndHandleTest {

    private static Service service;

    private static Handle handle = new DefaultHandle();

    private static RestApiService restApiService = new DefaultRestApiService();

    public static void main(String[] args) {

        service = Service.ignite();

        port(3770);

        restApiService.createGetRequestService("/sina", handle, "name", "age");

        restApiService.createPutRequestService("/update", handle, Person.class,
                "id");

        restApiService.createPutRequestService("/update1", handle, "myString",
                Person.class, "id");

    }

}
